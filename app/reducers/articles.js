import { news } from '../constants/news';
import { GET_ARTICLE } from '../actions/actionsTypes';

const initialState = {
	articles: news,
	currentArticle: 1
};

export default function articlesReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ARTICLE: {
			return { ...state, currentArticle: action.payload - 1 };
		}
		default:
			return state;
	}
}
