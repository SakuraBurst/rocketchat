import { StyleSheet } from 'react-native';

import sharedStyles from '../Styles';

export default StyleSheet.create({
	serverName: {
		...sharedStyles.textSemibold,
		fontSize: 20,
		marginBottom: 4
	},
	serverUrl: {
		...sharedStyles.textRegular,
		fontSize: 14,
		marginBottom: 24
	},
	registrationText: {
		fontSize: 14,
		...sharedStyles.textAlignCenter,
		...sharedStyles.textRegular
	},
	alignItemsCenter: {
		alignItems: 'center'
	},
	justifyContentsCenter: {
		display: 'flex',
		height: '100%',
		justifyContent: 'center'
	}
});
