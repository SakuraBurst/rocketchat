import { StyleSheet } from 'react-native';

import sharedStyles from '../Styles';

export default StyleSheet.create({
	sectionSeparatorBorder: {
		...sharedStyles.separatorVertical,
		height: 36
	},
	listPadding: {
		paddingVertical: 0,
		paddingHorizontal: 5
	},
	infoContainer: {
		backgroundColor: 'white',
		height: 100,
		marginTop: 20,
		padding: 10
	},
	infoText: {
		fontSize: 14,
		...sharedStyles.textRegular
	},
	otpText: {
		marginTop: 15,
		fontSize: 19,
		...sharedStyles.textBold,
		color: '#005C9D',
		marginBottom: 10
	}
});
