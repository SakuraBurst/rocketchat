import { StyleSheet } from 'react-native';

import sharedStyles from '../Styles';

export default StyleSheet.create({
	sectionSeparatorBorder: {
		...sharedStyles.separatorVertical,
		height: 36
	},
	listPadding: {
		paddingVertical: 0,
		paddingHorizontal: 5
	},
	infoContainer: {
		padding: 15
	},
	infoText: {
		fontSize: 14,
		...sharedStyles.textRegular
	},
	thickText: {
		marginTop: 15,
		fontSize: 19,
		...sharedStyles.textBold,
		color: 'black'
	}
});
