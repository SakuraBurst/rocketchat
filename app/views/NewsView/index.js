import React from 'react';
import {
	View,
	Linking,
	ScrollView,
	Text,
	Share,
	Image,
	TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-navigation';

import { logout as logoutAction } from '../../actions/login';
import { selectServerRequest as selectServerRequestAction } from '../../actions/server';
import { toggleCrashReport as toggleCrashReportAction } from '../../actions/crashReport';
import { SWITCH_TRACK_COLOR, themes } from '../../constants/colors';
import { DrawerButton, CloseModalButton } from '../../containers/HeaderButton';
import StatusBar from '../../containers/StatusBar';
import ListItem from '../../containers/ListItem';
import { DisclosureImage } from '../../containers/DisclosureIndicator';
import Separator from '../../containers/Separator';
import I18n from '../../i18n';
import RocketChat, { CRASH_REPORT_KEY } from '../../lib/rocketchat';
import {
	getReadableVersion,
	getDeviceModel,
	isAndroid
} from '../../utils/deviceInfo';
import scrollPersistTaps from '../../utils/scrollPersistTaps';
import { showErrorAlert, showConfirmationAlert } from '../../utils/info';
import styles from './styles';
import sharedStyles from '../Styles';
import { loggerConfig, analytics } from '../../utils/log';
import { withTheme } from '../../theme';
import { themedHeader } from '../../utils/navigation';
import SidebarView from '../SidebarView';
import { withSplit } from '../../split';
import { appStart as appStartAction } from '../../actions';
import { onReviewPress } from '../../utils/review';
import { getUserSelector } from '../../selectors/login';
import Carousel from '../../containers/Carousel';
import { news } from '../../constants/news';
import setArticle from '../../actions/articles';
import { CustomIcon } from '../../lib/Icons';
import OneTimePasswordView from '../OneTimePasswordView';

const SectionSeparator = React.memo(({ theme }) => (
	<View
		style={[
			styles.sectionSeparatorBorder,
			{
				borderColor: themes[theme].separatorColor,
				backgroundColor: themes[theme].auxiliaryBackground
			}
		]}
	/>
));
SectionSeparator.propTypes = {
	theme: PropTypes.string
};

const ItemInfo = React.memo(({ info, theme }) => (
	<View
		style={[
			styles.infoContainer,
			{ backgroundColor: themes[theme].auxiliaryBackground }
		]}
	>
		<Text style={[styles.infoText, { color: themes[theme].infoText }]}>
			{info}
		</Text>
	</View>
));
ItemInfo.propTypes = {
	info: PropTypes.string,
	theme: PropTypes.string
};

class NewsView extends React.Component {
	static navigationOptions = ({ navigation, screenProps }) => ({
		...themedHeader(screenProps.theme),
		headerLeft: screenProps.split ? (
			<CloseModalButton navigation={navigation} testID="news-view-close" />
		) : (
			<DrawerButton navigation={navigation} />
		),
		title: 'Новости'
	});

	static propTypes = {
		navigation: PropTypes.object,
		server: PropTypes.object,
		allowCrashReport: PropTypes.bool,
		toggleCrashReport: PropTypes.func,
		theme: PropTypes.string,
		split: PropTypes.bool,
		logout: PropTypes.func.isRequired,
		selectServerRequest: PropTypes.func,
		token: PropTypes.string,
		appStart: PropTypes.func
	};

	navigateToScreen = (screen, id) => {
		const { navigation, currentArticle } = this.props;
		navigation.navigate(screen);
		currentArticle(id);
	};

	sendEmail = async () => {
		const subject = encodeURI('React Native App Support');
		const email = encodeURI('support@rocket.chat');
		const description = encodeURI(`
			version: ${getReadableVersion}
			device: ${getDeviceModel}
		`);
		try {
			await Linking.openURL(
				`mailto:${email}?subject=${subject}&body=${description}`
			);
		} catch (e) {
			showErrorAlert(
				I18n.t('error-email-send-failed', { message: 'support@rocket.chat' })
			);
		}
	};

	shareApp = () => {
		Share.share({ message: isAndroid ? PLAY_MARKET_LINK : APP_STORE_LINK });
	};

	renderDisclosure = () => {
		const { theme } = this.props;
		return <DisclosureImage theme={theme} />;
	};

	render() {
		const { split, theme, news, navigation } = this.props;
		const NewsColumn = news.map((a, i) => (
			<TouchableOpacity
				activeOpacity={1}
				onPress={() => this.navigateToScreen('ArticleView', a.id)}
			>
				<View
					style={{
						marignHorizontal: 20,
						backgroundColor: 'white',
						marginBottom: 10,
						height: 90,
						display: 'flex',
						justifyContent: 'space-between',
						flex: 1,
						flexDirection: 'row',
						position: 'relative'
					}}
				>
					<Image source={{ uri: a.pictureUrl }} style={{ height: 90, width: 80 }} />
					<View
						style={{
							flex: 1,
							paddingTop: 10,
							paddingRight: 10,
							paddingLeft: 10,
							justifyContent: 'space-between'
						}}
					>
						<Text
							style={{
								color: 'black',
								fontSize: 10
							}}
						>
							{a.title}
						</Text>
						<View
							style={{
								display: 'flex',
								flexDirection: 'row',
								justifyContent: 'space-between',
								paddingBottom: 5
							}}
						>
							<Text>{a.date}</Text>
							<View style={{ display: 'flex', flexDirection: 'row' }}>
								<CustomIcon
									name="star"
									style={{ color: '#005C9D', marginRight: 10 }}
									size={20}
								/>
								<CustomIcon name="share" size={20} style={{ color: '#005C9D' }} />
							</View>
						</View>
					</View>
				</View>
			</TouchableOpacity>
		));
		return (
			<SafeAreaView
				style={[
					sharedStyles.container,
					{ backgroundColor: themes[theme].auxiliaryBackground }
				]}
				testID="news-view"
				forceInset={{ vertical: 'never' }}
			>
				<StatusBar theme={theme} />
				<ScrollView
					{...scrollPersistTaps}
					contentContainerStyle={styles.listPadding}
					showsVerticalScrollIndicator={false}
					testID="news-view-list"
				>
					{split ? (
						<>
							<Separator theme={theme} />
							<SidebarView theme={theme} />
							<SectionSeparator theme={theme} />
							<ListItem
								title={I18n.t('Profile')}
								onPress={() => this.navigateToScreen('ProfileView')}
								showActionIndicator
								testID="settings-profile"
								right={this.renderDisclosure}
								theme={theme}
							/>
						</>
					) : null}

					<Text style={styles.thickText}>Последние</Text>
					<Carousel items={news} />
					<Text style={{ ...styles.thickText, marginBottom: 10, paddingLeft: 10 }}>
						Все новости
					</Text>
					<View style={{ padding: 10 }}>{NewsColumn}</View>
				</ScrollView>
			</SafeAreaView>
		);
	}
}

const mapStateToProps = (state) => ({
	server: state.server,
	token: getUserSelector(state).token,
	allowCrashReport: state.crashReport.allowCrashReport,
	news: state.articlesReducer.articles
});

const mapDispatchToProps = (dispatch) => ({
	logout: () => dispatch(logoutAction()),
	selectServerRequest: (params) => dispatch(selectServerRequestAction(params)),
	toggleCrashReport: (params) => dispatch(toggleCrashReportAction(params)),
	appStart: (...params) => dispatch(appStartAction(...params)),
	currentArticle: (article) => dispatch(setArticle(article))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withTheme(withSplit(NewsView)));
