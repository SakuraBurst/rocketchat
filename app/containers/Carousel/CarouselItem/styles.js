import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
	slide: {
		marginRight: 15,
		flex: 1,
		justifyContent: 'center',
		maxWidth: '80%',
		flexBasis: '80%',
		height: 140
	},
	image: {
		width: '100%',
		display: 'flex',
		alignItems: 'center',
		alignContent: 'center',
		justifyContent: 'flex-end',
		height: '100%'
	},
	slideText: {
		width: '100%',
		textAlign: 'left',
		fontSize: 14,
		color: 'white',
		padding: 10
	}
});

export default styles;
