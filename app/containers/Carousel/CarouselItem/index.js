import React from 'react';
import {
	View,
	Text,
	Image,
	ImageBackground,
	TouchableOpacity
} from 'react-native';

import { styles } from './styles';
import { useDispatch } from 'react-redux';
import setArticle from '../../../actions/articles';
import Navigation from '../../../lib/Navigation';

export const Slide = (props) => {
	const { title, picture, last, id } = props;
	const dispatch = useDispatch();
	function redirect(id) {
		Navigation.navigate('ArticleView');
		dispatch(setArticle(id));
	}
	return (
		<View
			style={last ? { ...styles.slide, marginRight: 0 } : { ...styles.slide }}
		>
			<TouchableOpacity activeOpacity={1} onPress={() => redirect(id)}>
				<ImageBackground
					imageStyle={{ borderRadius: 5 }}
					style={styles.image}
					source={{
						uri: picture
					}}
				>
					<Text style={{ ...styles.slideText }}>{title}</Text>
				</ImageBackground>
			</TouchableOpacity>
		</View>
	);
};

export default Slide;
